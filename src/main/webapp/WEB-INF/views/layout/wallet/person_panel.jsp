<%@ page import="com.connectto.communicator.model.project.account.User" %>
<%@ page import="com.connectto.communicator.model.project.account.lcp.UserProfile" %>
<%@ page import="com.connectto.communicator.model.project.general.WalletSetup" %>
<%@ page import="com.connectto.general.util.ConstantGeneral" %>
<%@ page import="com.connectto.wallet.model.wallet.lcp.TransactionType" %>
<%@ page import="java.util.List" %>
<%--
  Created by IntelliJ IDEA.
  User: htdev001
  Date: 9/2/14
  Time: 10:15 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%
    User user = (User) session.getAttribute(ConstantGeneral.SESSION_USER);
    WalletSetup walletSetup = user.getPartition().getWalletSetup();
    List<TransactionType> availableCards = walletSetup.parseAvailableCards();
%>

<s:if test="#session.confirmed != null && #session.confirmed">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="<%=request.getContextPath()%>/css/wallet/wallet_person_panel.css">

    <script type="text/javascript" src="<%=request.getContextPath()%>/js/general/general_api.js"></script>
    <script type="text/javascript" src="<%=request.getContextPath()%>/js/wallet/freeze_charge_api.js"></script>
    <script type="text/javascript" src="<%=request.getContextPath()%>/js/wallet/freeze_charge_api_test.js"></script>


    <script type="text/javascript">

        function run_test (){
           // alert('send_money_check_tax_test');
//            send_money_check_tax_test();
//            send_money_preview_test();
            //send_money_make_payment_test();//+
//            send_money_approve_test();//+
//            send_money_reject_test();
//
//            request_transaction_check_tax_test();
//            request_transaction_preview_test();
//            request_transaction_make_payment_test();//+
//            request_transaction_approve_test();
//            request_transaction_reject_test();//+
//
//            load_available_currencies_test();
//            load_pending_transactions_test();//+
//            load_completed_transactions_test(); //+

         ///   charge_amount_from_wallet_test()
        }

        $(document).ready(function () {

            //run_test();

            close_menu();
            show_menu();
            log_out_wallet();


        })


        function log_out_wallet() {
            $(".close_wallet").click(function () {
                goToAction('<s:property value="#session.session_wallet.currentLocation.previousUrl"/>')
            })
        }
        function close_menu() {
            $(".close_menu").click(function () {
                if ($(".aside").hasClass("expand")) {
                    $(".aside").removeClass("expand")
                }
            })
        }
        function show_menu() {
            $(".menu_person_panel").click(function () {
                $(".aside").toggleClass("expand")
            })
        }


    </script>


    <aside class="aside">
        <div class="row text-center">


            <div class="name_surname_menu">
                <div class="close_menu">
                    <img src="<%=request.getContextPath()%>/img/wallet/icon/close_wallet_menu.png" alt="avatar"/>

                </div>
                <div style="clear: both;"></div>
                <div class="name_surname_menu_parent">
                    <div class="avatar_div_menu">
                        <s:if test="%{isUserThumbFileExist(#session.session_user.id, #session.session_user.photoData.fileName)}">
                            <img src="<s:property value='%{getUserThumbImg(#session.session_user.id, #session.session_user.photoData.fileName)}'/>"/>
                        </s:if>
                        <s:else>
                            <img src="<%=request.getContextPath()%>/img/wallet/icon/avatar_icon.png" alt="avatar"/>
                        </s:else>
                    </div>
                    <div style="clear: both;"></div>
                    <div class="name_surname_div_menu" onclick="goToAction('home_wallet.htm')">
                        <s:property value="#session.session_user.name"/> <s:property
                            value="#session.session_user.surname"/>

                    </div>

                </div>

            </div>


        </div>

        <div class="row">

            <div class="left_menu_icon">
                <div class="left_menu_img">
                    <img onclick="goToAction('send-money.htm')"
                         src="<%=request.getContextPath()%>/img/wallet/icon/send_money.png" alt="avatar"/>
                </div>
                <span onclick="goToAction('send-money.htm')" class="money">
                    <s:text name="wallet.login.send.money">Send money</s:text>
                </span>

            </div>
            <div class="left_menu_icon bordered">
                <div class="left_menu_img">
                    <img onclick="goToAction('request-transaction.htm')"
                         src="<%=request.getContextPath()%>/img/wallet/icon/requst_transaction.png" alt="avatar"/>
                </div>
                <span onclick="goToAction('request-transaction.htm')" class="request">
                    <s:text name="wallet.login.request.transaction">Request transaction</s:text>
                </span>

            </div>

            <div class="left_menu_icon">
                <div class="left_menu_img">
                    <img onclick="goToAction('pending-transaction.htm')" style="width: 75%"
                         src="<%=request.getContextPath()%>/img/wallet/icon/pending_transaction.png" alt="avatar"/>
                </div>
                <span onclick="goToAction('pending-transaction.htm')" class="pending ">
                     <s:text name="wallet.login.pending.transaction">Pending transaction</s:text>
                </span>

            </div>
            <div class="left_menu_icon bordered">
                <div class="left_menu_img">
                    <img onclick="goToAction('transactions.htm')" style="width: 75%"
                         src="<%=request.getContextPath()%>/img/wallet/icon/complited_transaction.png" alt="avatar"/>
                </div>
                <span onclick="goToAction('transactions.htm')" class="completed">
                    <s:text name="wallet.login.complited.transaction">Completed transaction</s:text>
                </span>

            </div>
            <%if(availableCards != null && availableCards.size() > 0){ %>
                <div class="left_menu_icon text-center">
                    <span onclick="goToAction('credit-cards-view.htm')" class="coming_soon">
                        <s:text name="wallet.login.card.transaction"> Credit Card Transaction</s:text>
                    </span>
                </div>
            <%}%>
        </div>

    </aside>

    <div class="person_panel container-fluid">
        <div class="row">
            <div class="header_logo">
                <div class="menu_person_panel">
                    <img src="<%=request.getContextPath()%>/img/wallet/icon/menu_icon.png" alt="menu"/>
                </div>
                <div class="header_logo_img col-lg-3 col-md-4 col-sm-5 col-xs-8">
                    <img src="<%=request.getContextPath()%>/img/wallet/logos/connecttotv_wallet.png" alt="logo"/>
                </div>
                <div class="log_out_div">
                    <div class="log_out" id="logout">
                        <img src="<%=request.getContextPath()%>/img/wallet/icon/wallet_log_out.png" alt="log_out">
                        <s:text name="wallet.log.out">Log Out</s:text>

                    </div>
                    <s:if test="%{#session.session_wallet.currentLocation.previousUrl != null}">
                        <div class="close_wallet">
                            <img src="<%=request.getContextPath()%>/img/wallet/icon/wallet_close.png"
                                 alt="close_wallet">
                            <s:text name="wallet.close.wallet">Close Wallet</s:text>

                        </div>
                    </s:if>

                    <div style="clear: both"></div>
                </div>
            </div>

                <%--name-surname for device--%>
            <div class="row text-center">
                <div class="name_surname_device">
                    <div class="avatar_div">
                        <s:if test="%{isUserThumbFileExist(#session.session_user.id, #session.session_user.photoData.fileName)}">
                            <img src="<s:property value='%{getUserThumbImg(#session.session_user.id, #session.session_user.photoData.fileName)}'/>"/>
                        </s:if>
                        <s:else>
                            <img src="<%=request.getContextPath()%>/img/wallet/icon/avatar_icon.png" alt="avatar"/>
                        </s:else>
                    </div>
                    <div class="name_surname_div" onclick="goToAction('home_wallet.htm')">
                        <s:property value="#session.session_user.name"/> <s:property
                            value="#session.session_user.surname"/>
                    </div>
                </div>
            </div>


            <div class="row">
                <div class="person_panel_parent">

                    <ul class="person_panel_ul">
                        <li class="name_surname_parent_li">
                            <div class="avatar_div" onclick="goToAction('home_wallet.htm')">
                                <s:if test="%{isUserThumbFileExist(#session.session_user.id, #session.session_user.photoData.fileName)}">
                                    <img src="<s:property value='%{getUserThumbImg(#session.session_user.id, #session.session_user.photoData.fileName)}'/>"/>
                                </s:if>
                                <s:else>
                                    <img src="<%=request.getContextPath()%>/img/wallet/icon/avatar_icon.png"
                                         alt="avatar"/>
                                </s:else>
                            </div>
                            <div class="name_surname_div" onclick="goToAction('home_wallet.htm')">
                                <s:property value="#session.session_user.name"/> <s:property
                                    value="#session.session_user.surname"/>
                            </div>
                        </li>
                        <li class="send_money_parent_li">
                            <div class="money_envelop">
                                <img onclick="goToAction('send-money.htm')"
                                     src="<%=request.getContextPath()%>/img/wallet/icon/send_money.png"
                                     alt="send money"/>
                            </div>
                            <span onclick="goToAction('send-money.htm')" class="send_money_div">

                                    <s:text name="wallet.login.send.money">Send money</s:text>


                            </span>
                                <%--<div class="selected_nav"></div>--%>
                        </li>
                        <li class="request_envelop_parent_li">
                            <div class="request_envelop">
                                <img onclick="goToAction('request-transaction.htm')"
                                     src="<%=request.getContextPath()%>/img/wallet/icon/requst_transaction.png"
                                     alt="request transaction"/>
                            </div>
                            <span onclick="goToAction('request-transaction.htm')" class="send_request_div">
                                    <s:text name="wallet.login.request.transaction">Request transaction</s:text>
                            </span>

                        </li>
                        <li class="pending_envelop_parent_li">
                            <div class="pending_envelop">
                                <img class="person_img"
                                     onclick="goToAction('pending-transaction.htm')"
                                     src="<%=request.getContextPath()%>/img/wallet/icon/pending_transaction.png"
                                     alt="pending transaction"/>
                            </div>
                            <span onclick="goToAction('pending-transaction.htm')" class="send_pending_div">
                                    <s:text name="wallet.login.pending.transaction">Pending transaction</s:text>
                            </span>

                        </li>
                        <li class="completed_envelop_parent_li">
                            <div class="completed_envelop">

                                <img class="person_img"
                                     onclick="goToAction('transactions.htm')"
                                     src="<%=request.getContextPath()%>/img/wallet/icon/complited_transaction.png"
                                     alt="pending transaction"/>
                            </div>
                            <span onclick="goToAction('transactions.htm')" class="send_completed_div">

                                    <s:text name="wallet.login.complited.transaction">Completed transaction</s:text>

                            </span>

                        </li>
                        <%if(availableCards != null && availableCards.size() > 0){ %>
                            <li>
                                <div class="coming_soon_div">
                                      <span onclick="goToAction('credit-cards-view.htm')" class="credit_card_span">
                                              <s:text name="wallet.login.card.transaction"> Credit Card Transaction</s:text>
                                      </span>
                                </div>
                            </li>
                        <%}%>
                    </ul>
                </div>
            </div>
        </div>


    </div>

</s:if>
<s:else>
    <link rel="stylesheet" href="<%=request.getContextPath()%>/css/wallet/wallet_home.css" media="screen">

    <script src='https://www.google.com/recaptcha/api.js'></script>


    <script>


        function footerPlace() {
            var window_width = $(window).width();
            var footer_height = $(".footer").outerHeight(true);
            console.log("foter height", footer_height);
            var window_height = $(window).height();
            console.log(window_height);
            var document_height = $(document).height();
            console.log(document_height);
            var content = document_height - footer_height;
            console.log("content", content);
            var margin = (window_height - $(".content").height()) / 2;


            if (window_height < document_height) {
                $(".content").css("margin-top", "30px");
                $(".main").css("height", "auto");
            }
            else {
                $(".content").css("margin-top", margin + "px");
                $(".main").css("height", content);
            }
        }

        function re_enter_input() {


            $('.input_div input').focus(function () {
                $(this).css("border-color", "#27ccc0");
            });
            $('.input_div input').blur(function () {
                $(this).css("border-color", "#555965");
            });
        };
            $(document).ready(function () {

                <%--alert('<%=((User)session.getAttribute(ConstantGeneral.SESSION_USER)).getId()%>')--%>
                footerPlace();
                re_enter_input();
                show_hide_password();

                $(window).resize(function () {
                    footerPlace()
                });


            });

            var title = function () {
                var a = $('.eye').css('background-image');
                if (~a.indexOf("password_eye.png")) {
                    $('.eye ').attr("title", "show password")
                }
                if (~a.indexOf("password_eye_closed.png")) {
                    $('.eye').attr("title", "hide password")
                }
            };

            function show_hide_password() {
                $('.eye').click(function () {
                    var back_img_text = $('.eye').css('background-image');
                    console.log("achqi nkar", back_img_text);
                    if (~back_img_text.indexOf("password_eye.png")) {
                        $('.eye').css('background', " url(<%=request.getContextPath()%>/img/wallet/icon/password_eye_closed.png) no-repeat  center");
                        $('.input_div input').attr('type', 'password');
                        title();
                    }
                    if (~back_img_text.indexOf("password_eye_closed.png")) {
                        $('.eye').css('background', " url(<%=request.getContextPath()%>/img/wallet/icon/password_eye.png) no-repeat  center");
                        $('.input_div input').attr('type', 'text');
                        title();
                    }
                });
            };


    </script>
    <div class="container-fluid">
        <div class="row">
            <div class=" row main">
                <div>--<s:actionerror/>--</div>


                <div class="row content">
                    <div class="  col-lg-8 col-md-7 col-sm-6 col-xs-12">
                        <div class="logo_div col-lg-8" autofocus>
                            <img src="<%=request.getContextPath()%>/img/wallet/logos/connecttotv_wallet.png"
                                 alt="logo"/>
                        </div>
                        <div style="clear: both"></div>
                        <div class="login_type  ">
                            <div class="text_parent centered">
                                <div class="logo_img">
                                    <img data-index="1"
                                         src='<s:property value="#session.session_url_partition.partitionServerUrl"/><s:property value="#session.session_url_partition.partitionLogoDirectory"/><s:property value="#session.session_url_partition.logoPath"/>'
                                         alt='<s:property value='#session.session_url_partition.name'/>'/>
                                </div>
                                <div class="vshoo_type">
                                    <div><s:property value='#session.session_url_partition.name'/></div>
                                </div>
                                <div class="vshoo_type">

                                    <% Integer profileTsm = (Integer) session.getAttribute(ConstantGeneral.SESSION_URL_TSM_TYPE_KEY);%>
                                    <%if(profileTsm != null){ %>
                                        <% if(UserProfile.VSHOO_CUSTOM.getKey() == profileTsm){%>
                                            <div><s:text name="label.vshoo.Customer">Customer</s:text></div>
                                        <%} else if(UserProfile.DRIVER.getKey() == profileTsm){%>
                                            <div><s:text name="label.vshoo.Driver">Driver</s:text></div>
                                        <%} else if(UserProfile.COMPANY.getKey() == profileTsm){%>
                                            <div><s:text  name="label.vshoo.Company">Company administrator</s:text></div>
                                        <%} else if(UserProfile.LOCATION.getKey() == profileTsm){%>
                                            <div><s:text  name="label.vshoo.Company.Location">Company location administrator</s:text></div>
                                        <%} %>
                                    <%} %>
                                </div>
                                <div style="clear: both"></div>
                            </div>
                        </div>
                    </div>

                        <%--not loginet page--%>
                    <div class="right text-center   col-lg-4 col-md-5 col-sm-6 col-xs-12">
                        <div class="name_surname_parent_li">
                            <div class="avatar_div">
                                <s:if test="%{isUserThumbFileExist(#session.session_user.id, #session.session_user.photoData.fileName)}">
                                    <img src="<s:property value='%{getUserThumbImg(#session.session_user.id, #session.session_user.photoData.fileName)}'/>"/>
                                </s:if>
                                <s:else>
                                    <img src="<%=request.getContextPath()%>/img/wallet/icon/avatar_icon.png"
                                         alt="avatar"/>
                                </s:else>
                            </div>
                            <div class="name_surname_div">
                                <s:property value="#session.session_user.name"/> <s:property
                                    value="#session.session_user.surname"/>
                            </div>
                        </div>

                        <form id="wallet_login" action="confirm_password_wallet.htm" method="post" autocomplete="nope">
                            <div class="input_div centered col-lg-10 col-md-10 col-sm-10 col-xs-10">
                                <div class="lock"></div>
                                <div class="eye"></div>
                                <input type="password" style="display: none">
                                <input type="password" name="password" data-index="1" autocomplete="nope"
                                       placeholder="<s:text name="wallet.general.placeholder.text">Re-Enter Password</s:text>"
                                       required/>
                                <span class="forError"><s:fielderror name="password" fieldName="password"/></span>
                            </div>
                            <div style="clear: both;height: 50px"></div>
                            <div class="centered chapta col-lg-10 col-md-10 col-sm-10 col-xs-10">
                                <div class="g-recaptcha" data-theme="dark"
                                     data-sitekey='<s:property value="#session.session_url_partition.partitionSetupOnGmail.recaptchaClientKey"/>'></div>
                            </div>
                            <div style="clear: both;height: 50px"></div>
                            <div class="centered col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="centered button_div col-lg-10 col-md-10 col-sm-10 col-xs-10">
                                    <button class="btn login_button" id="wallet_login_submit" onclick="loader_show()"
                                            data-index="3">
                                        <s:text name="wallet.general.button.login">log in</s:text>
                                    </button>
                                </div>
                            </div>
                        </form>

                    </div>
                    <div style="clear:both; height: 30px"></div>
                </div>

            </div>
        </div>
    </div>

</s:else>