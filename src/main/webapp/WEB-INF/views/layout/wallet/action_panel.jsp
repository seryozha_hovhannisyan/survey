<%--
  Created by IntelliJ IDEA.
  User: htdev001
  Date: 9/2/14
  Time: 10:18 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<s:if test="#session.confirmed != null && #session.confirmed">
    <script type="text/javascript">
        close_any_popup('.curent_bal_div');
        $("body").delegate(".close_bal",'click',function () {
            $(".curent_bal_div").hide();
        });
        $(document).keydown(function (e) {
            if (e.which == 27){
                $(".curent_bal_div").hide();
            }
        });
        function current_balance() {
            $.ajax({
                url: "current-balance-view.htm",
                type: "post",
                dataType: "json",
                async: true,
                success: function (data) {

                    if (data != null && data.responseDto.status == "SUCCESS") {
                        var wallet = data.userWallet;
                        var current_balanc = $('<div class="curent_bal_div"><div class="close_bal"></div><div style="clear: both"></div><div class="balance_text"><s:text name="wallet.login.your.current.balance.is">Your Current Balance is</s:text>' +
                                '<div>' + wallet.money + '</div></div><div class="balance_text"><s:text name="wallet.login.your.frozen.amount.is">Amount in hold in your account</s:text><div>' + wallet.frozenAmount + '</div>' +
                                '</div><div class="balance_text"><s:text name="wallet.login.your.receiving.amount.is">Expected incoming transactions</s:text><div>' + wallet.receivingAmount + '</div></div>' +
                                '<div class="balance_text"><s:text name="wallet.login.your.currency.type.is">Your Currency Type is</s:text><div>' + wallet.currencyType + '</div></div></div>');
                        $("body").append(current_balanc)

                    } else {
                        alert(data.responseDto.status + " " + data.responseDto.messages);
                    }
                },
                error: function (e) {
                    console.log(e);
                }
            })
        }
    </script>
    <link rel="stylesheet" href="<%=request.getContextPath()%>/css/wallet/login/wallet_login.css" media="screen">
    <div class="row">
        <div class="current_button_div">
            <button class="btn" onclick="current_balance()">
                <img src="<%=request.getContextPath()%>/img/wallet/icon/balance_button_icon.png"
                     alt="balance icon"/>
                <s:text name="wallet.login.current.balance"> Current Balance</s:text>
            </button>
        </div>
    </div>
    <div class="row">
        <div class="hello_div_parent">
            <s:text name="info.welcome.wallet.hello">Hello</s:text>
            <div class="name_div">
                <s:property value="#session.session_user.name"/> <s:property value="#session.session_user.surname"/>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="user_text_div col-lg-4 col-md-5 col-sm-5 col-xs-8">
            <s:text name="wallet.login.hello.text">Welcome to your ConnectToWallet</s:text>
        </div>
    </div>


</s:if>

