<%@ page import="com.connectto.communicator.model.project.general.Partition" %>
<%@ page import="com.connectto.general.model.PartitionLCP" %>
<%@ page import="com.connectto.general.util.ConstantGeneral" %>
<!DOCTYPE html>
<%--
  Created by IntelliJ IDEA.
  User: htdev001
  Date: 6/5/14
  Time: 11:17 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%
    Partition partition = (Partition)session.getAttribute(ConstantGeneral.SESSION_URL_PARTITION);
    String partitionDns = PartitionLCP.getDNS(partition.getId());
%>
<html>
<head>

    <title><s:text name="wallet.general.title">Wallet</s:text></title>

    <link rel="icon"
          type="image/png"
          href="<%=request.getContextPath()%>/img/general/<%=partitionDns%>/favicon.png">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">

    <link href="<%=request.getContextPath()%>/css/general/bootstrap/css/bootstrap.css" rel="stylesheet">
    <link href="<%=request.getContextPath()%>/css/general/datepicker_bootstrap.css" rel="stylesheet">

    <%--<link href="http://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css" rel="stylesheet" type="text/css" />--%>
    <style type="text/css">
        .fix_bottom {
            position: absolute;
            bottom: 0;
        }
        .blur {
            -webkit-filter: blur(5px);
            -moz-filter: blur(5px);
            -o-filter: blur(5px);
            -ms-filter: blur(5px);
            filter: blur(5px);
        }
        .loader{
            position: absolute;
            width: 100%;
            top: 0px;
            left: 0px;
            left: 0px;
            z-index: 10;
            display: none;



        }
        .loader_img_div{
            position: absolute;
            height: 50px;
            width: 50px;
            top: 50%;
            z-index: 3;
            left: 50%;

        }
        .loader img{
            width: 100%;
            height: auto;
        }
    </style>
    <script type="text/javascript" src="<%=request.getContextPath()%>/js/general/jquery/jquery.js"></script>
    <script type="text/javascript" src="<%=request.getContextPath()%>/js/general/bootstrap/js/bootstrap.js"></script>
    <script type="text/javascript" src="<%=request.getContextPath()%>/js/general/bootstrap-datepicker.js"></script>
    <script type="text/javascript" src="<%=request.getContextPath()%>/js/general/general_api.js"></script>
    <script type="text/javascript" src="<%=request.getContextPath()%>/js/general/log.js"></script>
    <script type="text/javascript" src="<%=request.getContextPath()%>/js/general/general_api.js"></script>
    <script type="text/javascript" src="<%=request.getContextPath()%>/js/general/log.js"></script>
    <script type="text/javascript" src="<%=request.getContextPath()%>/js/general/jquery/jquery-ui.js"></script>

    <script type="text/javascript">
        function goToAction(href) {
            window.location = href;
        }

        function close_any_popup(selector) {
            $(document).click(function (e) {
                if ($(e.target).closest(selector).length != 0) return false;
                console.log("e.target",$(e.target));
                console.log("e.target lengt",$(e.target).closest(selector).length);
                $(selector).hide();
            });

        }

        function only_number(selector) {
            $(selector).keydown(function (e) {
                // Allow: backspace, delete, tab, escape, enter and .
                if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                            // Allow: Ctrl+A
                        (e.keyCode == 65 && e.ctrlKey === true) ||
                            // Allow: Ctrl+C
                        (e.keyCode == 67 && e.ctrlKey === true) ||
                            // Allow: Ctrl+X
                        (e.keyCode == 88 && e.ctrlKey === true) ||
                            // Allow: home, end, left, right
                        (e.keyCode >= 35 && e.keyCode <= 39)) {
                    // let it happen, don't do anything
                    return;
                }
                // Ensure that it is a number and stop the keypress
                if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                    e.preventDefault();
                }
            });
        }
        function selected_nav (selector){
            $(selector).append($("<div class='selected_nav'></div>"))
        }
        function loader_show() {
            var loader_height = $(document).height();
            $(".loader").css("height",loader_height+"px").show();
            $(".div_for_blur").addClass("blur");
        }

        function loader_hide() {
            $(".loader").hide();
            $(".div_for_blur").removeClass("blur");
        }

        function footerPlace() {
            var doc_height = Math.max(
                    document.body.scrollHeight, document.documentElement.scrollHeight,
                    document.body.offsetHeight, document.documentElement.offsetHeight,
                    document.body.clientHeight, document.documentElement.clientHeight
            );
            var win_height = $(window).height();
            console.log("doc_height",doc_height,"win_height",win_height);
            if (win_height < doc_height) {
                $(".footer").removeClass("fix_bottom")
            }
            else {
                $(".footer").addClass("fix_bottom")
            }
        }
        $(document).ready(function () {
            footerPlace();
            $(window).resize(function () {
                footerPlace();
            });

        })

    </script>

</head>
<body>
<div class="div_for_blur">
<tiles:insertAttribute name="person_panel"/>
<tiles:insertAttribute name="action_panel"/>
<tiles:insertAttribute name="accounts"/>
<tiles:insertAttribute name="footer_panel"/>
</div>

<div class="loader">
    <div class="loader_img_div">
        <img class="center-block loading" src="<%=request.getContextPath()%>/img/general/login/loading.gif"
             alt="loading..."/>
    </div>
</div>
</body>
</html>