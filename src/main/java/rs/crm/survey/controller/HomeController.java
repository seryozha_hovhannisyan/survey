package rs.crm.survey.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.time.LocalDateTime;
import java.util.Properties;

@Controller
class HomeController {

    @RequestMapping("/")
    String index(Model model) {
        model.addAttribute("now", LocalDateTime.now());
        return "index";
    }

    @RequestMapping("/e")
    String e(Model model) {
        model.addAttribute("now", LocalDateTime.now());
        return "layout/start";
    }

    @RequestMapping("/f")
    String f(Model model) {
        model.addAttribute("now", LocalDateTime.now());
        return "layout/forms";
    }

    @RequestMapping("properties")
    @ResponseBody
    Properties properties() {
        return System.getProperties();
    }
}
