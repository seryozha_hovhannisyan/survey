package rs.crm.survey.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import rs.crm.survey.dataaccess.sevice.IInputManager;
import rs.crm.survey.dataaccess.domain.Input;
import rs.crm.survey.dataaccess.exception.EntityNotFoundException;
import rs.crm.survey.dataaccess.exception.InternalErrorException;
import rs.crm.survey.util.DomainInitializer;

@Controller
class ImputController {

    @Autowired
    IInputManager inputManager;

    @RequestMapping("/add-input")
    public String add(Model model) {
        Input input = DomainInitializer.initInput();
        try {
            inputManager.add(input);
            model.addAttribute("status", "add");
        } catch (Exception e) {
            model.addAttribute("status", e.getMessage());
        }

        return "pages/input";
    }

    ;

    @RequestMapping("/input-get-by-id")
    public String getById(@RequestParam(value = "id", required = false, defaultValue = "1") Long id, Model model) {
        model.addAttribute("status", "");
        try {
            Input input = inputManager.getById(id);
            model.addAttribute("status", "got " + input);
        } catch (InternalErrorException e) {
            model.addAttribute("status", e.getMessage());
        } catch (EntityNotFoundException e) {
            model.addAttribute("status", e.getMessage());
        }
        return "pages/input";
    }

    ;

    @RequestMapping("/input-getAll")
    public String getAll(Model model) {
        model.addAttribute("status", "");
        return "pages/input";
    }

    ;

    @RequestMapping("/input-update")
    public String update(Model model) {
        model.addAttribute("status", "");
        return "pages/input";
    }

    ;

    @RequestMapping("/input-delete")
    public String delet(Model model ) {
        model.addAttribute("status", "");
        return "pages/input";
    }

    ;
}
