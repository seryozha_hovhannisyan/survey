package rs.crm.survey.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import rs.crm.survey.dataaccess.domain.lcp.InputType;
import rs.crm.survey.dataaccess.sevice.IFormManager;
import rs.crm.survey.dataaccess.domain.Form;
import rs.crm.survey.dataaccess.exception.EntityNotFoundException;
import rs.crm.survey.dataaccess.exception.InternalErrorException;
import rs.crm.survey.util.DomainInitializer;

import java.util.List;

@Controller
class FormController {

    @Autowired
    IFormManager formManager;

    @RequestMapping("/form-add-view")
    public String addView(Model model) {
        return "layout/form/create";
    }

    @RequestMapping("/form-add")
    public String add(Model model) {
        Form form = DomainInitializer.initForm();
        try {
            formManager.add(form);
            model.addAttribute("status", "add");
        } catch (Exception e) {
            e.printStackTrace();
            model.addAttribute("status", e.getMessage());
        }

        return "pages/form";
    }


    @RequestMapping("/form-get-by-id")
    public String getById(@RequestParam(value = "id", required = false, defaultValue = "0") Long id, Model model) {
        model.addAttribute("status", "");
        try {
            Form form = formManager.getById(id);
            model.addAttribute("form", form);
        } catch (InternalErrorException e) {
            model.addAttribute("status", e.getMessage());
        } catch (EntityNotFoundException e) {
            model.addAttribute("status", e.getMessage());
        }
        return "layout/form/view/one";
    }

    @RequestMapping("/forms")
    public String getAll(Model model) {
        try {
            List<Form> forms = formManager.getAll();
            model.addAttribute("forms", forms);
        } catch (InternalErrorException e) {
            e.printStackTrace();
        }

        return "layout/form/view/all";
    }

    @RequestMapping("/form-update-view")
    public String updateView(@RequestParam(value = "id", required = false, defaultValue = "0") Long id, Model model) {
        model.addAttribute("status", "");
        try {
            Form form = formManager.getById(id);
            model.addAttribute("form", form);
        } catch (InternalErrorException e) {
            model.addAttribute("status", e.getMessage());
        } catch (EntityNotFoundException e) {
            model.addAttribute("status", e.getMessage());
        }
        return "layout/form/update";
    }

    @RequestMapping("/form-update")
    public String update(@RequestParam(value = "id", required = false, defaultValue = "0") Long id, Model model) {
        model.addAttribute("id",id);
        return "redirect:form-get-by-id";
    }

    @RequestMapping("/form-delete")
    public String delete(Model model ) {
        model.addAttribute("status", "");
        return "redirect:forms";
    }

}
