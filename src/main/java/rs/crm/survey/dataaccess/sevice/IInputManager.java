package rs.crm.survey.dataaccess.sevice;

import rs.crm.survey.dataaccess.domain.Input;
import rs.crm.survey.dataaccess.exception.EntityNotFoundException;
import rs.crm.survey.dataaccess.exception.InternalErrorException;

import java.util.List;

/**
 * Created by Serozh on 3/6/2016.
 */
public interface IInputManager {

    public void add(Input input) throws InternalErrorException;

    public Input getById(Long id) throws InternalErrorException, EntityNotFoundException;

    public List<Input> getAll(Long id) throws InternalErrorException;

    public void update(Input input) throws InternalErrorException, EntityNotFoundException;

    public void delet(Input input) throws InternalErrorException;


}
