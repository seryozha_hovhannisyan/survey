package rs.crm.survey.dataaccess.sevice.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import rs.crm.survey.dataaccess.sevice.IInputManager;
import rs.crm.survey.dataaccess.domain.Input;
import rs.crm.survey.dataaccess.exception.EntityNotFoundException;
import rs.crm.survey.dataaccess.exception.InternalErrorException;
import rs.crm.survey.dataaccess.repository.InputRepository;

import java.util.List;

/**
 * Created by Serozh on 3/6/2016.
 */

@Component("inputManager")
@Transactional(readOnly = true)
public class InputManagerImpl implements IInputManager {

    @Autowired
    private InputRepository inputRepository;

    @Override
    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public void add(Input input) throws InternalErrorException {
        inputRepository.save(input);
    }

    @Override
    public Input getById(Long id) throws InternalErrorException, EntityNotFoundException {
        return inputRepository.findOne(id);
    }

    @Override
    public List<Input> getAll(Long id) throws InternalErrorException {
        return inputRepository.findAll();
    }

    @Override
    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public void update(Input input) throws InternalErrorException, EntityNotFoundException {

        Input iOne = inputRepository.findOne(input.getId());
        if(iOne == null){
            throw new EntityNotFoundException("");
        }

        inputRepository.saveAndFlush(input);
    }

    @Override
    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public void delet(Input input) throws InternalErrorException {
        inputRepository.delete(input.getId());
    }
}
