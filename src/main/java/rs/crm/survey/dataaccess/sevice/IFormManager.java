package rs.crm.survey.dataaccess.sevice;

import rs.crm.survey.dataaccess.domain.Form;
import rs.crm.survey.dataaccess.exception.EntityNotFoundException;
import rs.crm.survey.dataaccess.exception.InternalErrorException;

import java.util.List;

/**
 * Created by Serozh on 3/6/2016.
 */
public interface IFormManager {

    public void add(Form input) throws InternalErrorException;

    public Form getById(Long id) throws InternalErrorException, EntityNotFoundException;

    public List<Form> getAll() throws InternalErrorException;

    public void update(Form input) throws InternalErrorException, EntityNotFoundException;

    public void delete(Form input) throws InternalErrorException;


}
