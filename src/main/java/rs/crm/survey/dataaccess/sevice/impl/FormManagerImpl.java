package rs.crm.survey.dataaccess.sevice.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import rs.crm.survey.dataaccess.sevice.IFormManager;
import rs.crm.survey.dataaccess.domain.Form;
import rs.crm.survey.dataaccess.exception.EntityNotFoundException;
import rs.crm.survey.dataaccess.exception.InternalErrorException;
import rs.crm.survey.dataaccess.repository.FormRepository;

import java.util.List;

/**
 * Created by Serozh on 3/6/2016.
 */

@Component("formManager")
@Transactional(readOnly = true)
public class FormManagerImpl implements IFormManager {

    @Autowired
    private FormRepository formRepository;

    @Override
    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public void add(Form form) throws InternalErrorException {
        formRepository.save(form);
    }

    @Override
    public Form getById(Long id) throws InternalErrorException, EntityNotFoundException {
        return formRepository.findOne(id);
    }

    @Override
    public List<Form> getAll() throws InternalErrorException {
        return formRepository.findAll();
    }

    @Override
    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public void update(Form form) throws InternalErrorException, EntityNotFoundException {

        Form iOne = formRepository.findOne(form.getId());
        if(iOne == null){
            throw new EntityNotFoundException("");
        }

        formRepository.saveAndFlush(form);
    }

    @Override
    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public void delete(Form form) throws InternalErrorException {
        formRepository.delete(form.getId());
    }
}
