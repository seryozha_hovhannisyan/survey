package rs.crm.survey.dataaccess.repository;

import org.springframework.data.repository.Repository;
import rs.crm.survey.dataaccess.domain.Form;

import java.util.List;

/**
 * Created by Serozh on 3/6/2016.
 */
public interface FormRepository extends Repository<Form, Long> {
    /*
        save
        findOne
        exists
        findAll
        count
        delete
        deleteAll
    */
    public void save(Form input) ;

    public Form findOne(Long id) ;

    public List<Form> findAll() ;

    public void saveAndFlush(Form input) ;

    public void delete(Long id) ;

}
