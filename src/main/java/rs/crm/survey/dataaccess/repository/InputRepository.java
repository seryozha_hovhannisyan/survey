package rs.crm.survey.dataaccess.repository;

import org.springframework.data.repository.Repository;
import rs.crm.survey.dataaccess.domain.Input;

import java.util.List;

/**
 * Created by Serozh on 3/6/2016.
 */
public interface InputRepository extends Repository<Input, Long> {
    /*
        save
        findOne
        exists
        findAll
        count
        delete
        deleteAll
    */
    public void save(Input input) ;

    public Input findOne(Long id) ;

    public List<Input> findAll() ;

    public void saveAndFlush(Input input) ;

    public void delete(Long id) ;

}
