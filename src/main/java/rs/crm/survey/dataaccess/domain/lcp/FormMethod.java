package rs.crm.survey.dataaccess.domain.lcp;

/**
 * Created by Arthur on 3/5/2016.
 */
public enum FormMethod {

    GET     (1, "get"),
    POST    (2, "post");


    FormMethod(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public static FormMethod getDefault() {
        return GET;
    }

    public static synchronized FormMethod valueOf(final int id) {
        for (FormMethod f : FormMethod.values()) {
            if (f.getId() == id) {
                return f;
            }
        }
        return null;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    private int id;
    private String name;
}
