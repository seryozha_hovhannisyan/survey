package rs.crm.survey.dataaccess.domain.lcp;

/**
 * Created by Serozh on 3/6/2016.
 */
public enum  InputCondition {

    LESS        (1,"less"),
    EQUAL       (2,"equal"),
    GREAT       (3,"great"),
    NOT_NULL    (4,"not null"),
    NULL        (5,"null");

    InputCondition(int id, String name) {
        this.id = id;
        this.name = name;
    }


    public static InputCondition getDefault() {
        return NOT_NULL;
    }

    public static synchronized InputCondition valueOf(final int id) {
        for (InputCondition c : InputCondition.values()) {
            if (c.getId() == id) {
                return c;
            }
        }
        return null;
    }

    public static synchronized InputCondition[] equalConditions(){
        return new InputCondition[]{InputCondition.EQUAL,InputCondition.GREAT,InputCondition.LESS};
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    private int id;
    private String name;

}
