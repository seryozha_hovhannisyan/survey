package rs.crm.survey.dataaccess.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Serozh on 3/6/2016.
 */
@Entity(name = "html_formset")
public class FieldSet implements Serializable {

    @Id
    @GeneratedValue
    private Long id;

    @Column(nullable = false)
    private String legend;

    /*@OneToMany(fetch = FetchType.LAZY, mappedBy = "fieldSet")*/
    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "fieldset_input",
            joinColumns = { @JoinColumn(name = "fieldset_id") },
            inverseJoinColumns = { @JoinColumn(name = "input_id") })
    private Set<Input> inputs;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "fieldset_select",
            joinColumns = { @JoinColumn(name = "fieldset_id") },
            inverseJoinColumns = { @JoinColumn(name = "select_id") })
    private Set<Select> selects;


    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "form_fieldset",
            joinColumns = { @JoinColumn(name = "fieldSet_id") },
            inverseJoinColumns = { @JoinColumn(name = "form_id") })
    private Set<Form> forms = new HashSet<>();

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "fieldset",cascade = CascadeType.ALL)
    private Set<Logic> logics;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLegend() {
        return legend;
    }

    public void setLegend(String legend) {
        this.legend = legend;
    }

    public Set<Input> getInputs() {
        return inputs;
    }

    public void setInputs(Set<Input> inputs) {
        this.inputs = inputs;
    }

    public Set<Form> getForms() {
        return forms;
    }

    public void setForms(Set<Form> forms) {
        this.forms = forms;
    }

    public Set<Select> getSelects() {
        return selects;
    }

    public void setSelects(Set<Select> selects) {
        this.selects = selects;
    }

    public Set<Logic> getLogics() {
        return logics;
    }

    public void setLogics(Set<Logic> logics) {
        this.logics = logics;
    }
}
