package rs.crm.survey.dataaccess.domain.lcp;

/**
 * Created by Arthur on 3/5/2016.
 */
public enum FormEnctype {

    APPLICATION_X_WWW_FORM_URL_ENCODED         (1,"application/x-www-form-urlencoded", "" ),
    MULTIPART_FORM_DATA (2,"multipart/form-data", "" ),
    TEXT_PLAIN          (3,"text/plain", "" ) ;

    FormEnctype(int id, String name, String description) {
        this.id = id;
        this.name = name;
        this.description = description;
    }

    public static FormEnctype getDefault() {
        return APPLICATION_X_WWW_FORM_URL_ENCODED;
    }

    public static synchronized FormEnctype valueOf(final int id) {
        for (FormEnctype e : FormEnctype.values()) {
            if (e.getId() == id) {
                return e;
            }
        }
        return null;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    private int id;
    private String name;
    private String description;
}
