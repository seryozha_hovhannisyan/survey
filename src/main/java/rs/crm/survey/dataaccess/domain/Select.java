package rs.crm.survey.dataaccess.domain;

import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Serozh on 3/6/2016.
 */
@Entity(name = "html_select")
public class Select implements Serializable {

    @Id
    @GeneratedValue
    private Long id;

    @Column(nullable = false, name = "selected", length = 1)
    @Type(type = "numeric_boolean")
    private boolean multiple;

    @Column(nullable = true)
    private int size;

    @Column(nullable = false)
    private String name;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "select",cascade = CascadeType.ALL)
    private Set<Option> options;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "select",cascade = CascadeType.ALL)
    private Set<AnswerSelect> answerSelects;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "form_select",
            joinColumns = { @JoinColumn(name = "select_id") },
            inverseJoinColumns = { @JoinColumn(name = "form_id") })
    private Set<Form> forms = new HashSet<Form>();

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "fieldset_select",
            joinColumns = { @JoinColumn(name = "select_id") },
            inverseJoinColumns = { @JoinColumn(name = "fieldset_id") })
    private Set<FieldSet> fieldSets = new HashSet<FieldSet>();

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "select",cascade = CascadeType.ALL)
    private Set<Logic> logics;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public boolean isMultiple() {
        return multiple;
    }

    public void setMultiple(boolean multiple) {
        this.multiple = multiple;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Option> getOptions() {
        return options;
    }

    public void setOptions(Set<Option> options) {
        this.options = options;
    }

    public Set<AnswerSelect> getAnswerSelects() {
        return answerSelects;
    }

    public void setAnswerSelects(Set<AnswerSelect> answerSelects) {
        this.answerSelects = answerSelects;
    }

    public Set<FieldSet> getFieldSets() {
        return fieldSets;
    }

    public void setFieldSets(Set<FieldSet> fieldSets) {
        this.fieldSets = fieldSets;
    }

    public Set<Form> getForms() {
        return forms;
    }

    public void setForms(Set<Form> forms) {
        this.forms = forms;
    }

    public Set<Logic> getLogics() {
        return logics;
    }

    public void setLogics(Set<Logic> logics) {
        this.logics = logics;
    }
}
