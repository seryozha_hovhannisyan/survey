package rs.crm.survey.dataaccess.domain;

import org.hibernate.annotations.Type;
import rs.crm.survey.dataaccess.domain.lcp.InputType;
//import rs.crm.survey.dataaccess.handler.BooleanConverter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Arthur on 3/5/2016.
 */
@Entity(name = "html_input")
public class Input implements Serializable {

    @Id
    @GeneratedValue
    private Long id;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "form_input",
            joinColumns = { @JoinColumn(name = "input_id") },
            inverseJoinColumns = { @JoinColumn(name = "form_id") })
    private Set<Form> forms = new HashSet<Form>();

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "fieldset_input",
            joinColumns = { @JoinColumn(name = "input_id") },
            inverseJoinColumns = { @JoinColumn(name = "fieldset_id") })
    private Set<FieldSet> fieldSets = new HashSet<FieldSet>();

    @Column(nullable = false, length = 2)
    private int section;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false,length = 2)
    @Enumerated(EnumType.ORDINAL)
    private InputType type;

    @Column(nullable = true)
    private String value;

    @Column(nullable = true, name = "default_value")
    private String defaultValue;

    @Column(nullable = true)
    private String label;

    @Column(nullable = true)
    private String placeholder;

    @Column(nullable = true, name = "disabled",length = 1)
    @Type(type =  "numeric_boolean")
    private boolean disabled;

    @Column(nullable = true)
    private int min;

    @Column(nullable = true)
    private int max;

    @Column(nullable = true, name = "max_length")
    private int maxLength;

    @Column(nullable = true)
    private String pattern;

    @Column(nullable = true, name = "readonly" ,length = 1)
    @Type(type =  "numeric_boolean")
    private Boolean readonly;

    //@Convert(converter = BooleanConverter.class)
    @Column(nullable = true, name = "required" ,length = 1)
    @Type(type =  "numeric_boolean")
    private Boolean required;

    @Column(nullable = true)
    private int size;

    @Column(nullable = true)
    private int step;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "input",cascade = CascadeType.ALL)
    private Set<Logic> logics;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "input",cascade = CascadeType.ALL)
    private Set<AnswerInput> answerInputs;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Set<Form> getForms() {
        return forms;
    }

    public void setForms(Set<Form> forms) {
        this.forms = forms;
    }

    public Set<FieldSet> getFieldSets() {
        return fieldSets;
    }

    public void setFieldSets(Set<FieldSet> fieldSets) {
        this.fieldSets = fieldSets;
    }

    public int getSection() {
        return section;
    }

    public void setSection(int section) {
        this.section = section;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public InputType getType() {
        return type;
    }

    public void setType(InputType type) {
        this.type = type;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getDefaultValue() {
        return defaultValue;
    }

    public void setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getPlaceholder() {
        return placeholder;
    }

    public void setPlaceholder(String placeholder) {
        this.placeholder = placeholder;
    }

    public boolean isDisabled() {
        return disabled;
    }

    public void setDisabled(boolean disabled) {
        this.disabled = disabled;
    }

    public int getMin() {
        return min;
    }

    public void setMin(int min) {
        this.min = min;
    }

    public int getMax() {
        return max;
    }

    public void setMax(int max) {
        this.max = max;
    }

    public int getMaxLength() {
        return maxLength;
    }

    public void setMaxLength(int maxLength) {
        this.maxLength = maxLength;
    }

    public String getPattern() {
        return pattern;
    }

    public void setPattern(String pattern) {
        this.pattern = pattern;
    }

    public Boolean getReadonly() {
        return readonly;
    }

    public void setReadonly(Boolean readonly) {
        this.readonly = readonly;
    }

    public Boolean getRequired() {
        return required;
    }

    public void setRequired(Boolean required) {
        this.required = required;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public int getStep() {
        return step;
    }

    public void setStep(int step) {
        this.step = step;
    }

    public Set<Logic> getLogics() {
        return logics;
    }

    public void setLogics(Set<Logic> logics) {
        this.logics = logics;
    }

    public Set<AnswerInput> getAnswerInputs() {
        return answerInputs;
    }

    public void setAnswerInputs(Set<AnswerInput> answerInputs) {
        this.answerInputs = answerInputs;
    }
}
