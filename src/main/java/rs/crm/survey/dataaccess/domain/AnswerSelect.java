package rs.crm.survey.dataaccess.domain;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by Serozh on 3/7/2016.
 */
@Entity(name = "answer_select")
public class AnswerSelect implements Serializable{

    @Id
    @GeneratedValue
    private Long id;

    @ManyToOne(optional = false,cascade = CascadeType.ALL)
    private Select select;

    @ManyToOne(optional = false,cascade = CascadeType.ALL)
    private AnswerForm answerForm;

    @Column(nullable = true)
    private String value;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Select getSelect() {
        return select;
    }

    public void setSelect(Select select) {
        this.select = select;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public AnswerForm getAnswerForm() {
        return answerForm;
    }

    public void setAnswerForm(AnswerForm answerForm) {
        this.answerForm = answerForm;
    }
}
