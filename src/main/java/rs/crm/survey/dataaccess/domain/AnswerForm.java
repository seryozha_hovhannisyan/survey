package rs.crm.survey.dataaccess.domain;

import rs.crm.survey.dataaccess.domain.lcp.FormEnctype;
import rs.crm.survey.dataaccess.domain.lcp.FormMethod;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Arthur on 3/5/2016.
 */
@Entity(name = "answer_form")
public class AnswerForm implements Serializable { 

    @Id
    @GeneratedValue
    private Long id; 

    @Column(nullable = false, name = "answered_by")
    private Long answeredBy;

    @Column(nullable = false, name = "checked_by")
    private Long checkedBy;

    @Column(nullable = false, name = "answered_at")
    @Temporal(TemporalType.DATE)
    private Date answeredAt;

    @Column(nullable = false, name = "checked_at")
    @Temporal(TemporalType.DATE)
    private Date checkedAt;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "answerForm",cascade = CascadeType.ALL)
    private Set<AnswerInput> answerInputs;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "answerForm",cascade = CascadeType.ALL)
    private Set<AnswerSelect> answerSelects;

    @ManyToOne(optional = false,cascade = CascadeType.ALL)
    private Form form;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getAnsweredBy() {
        return answeredBy;
    }

    public void setAnsweredBy(Long answeredBy) {
        this.answeredBy = answeredBy;
    }

    public Long getCheckedBy() {
        return checkedBy;
    }

    public void setCheckedBy(Long checkedBy) {
        this.checkedBy = checkedBy;
    }

    public Date getAnsweredAt() {
        return answeredAt;
    }

    public void setAnsweredAt(Date answeredAt) {
        this.answeredAt = answeredAt;
    }

    public Date getCheckedAt() {
        return checkedAt;
    }

    public void setCheckedAt(Date checkedAt) {
        this.checkedAt = checkedAt;
    }

    public Set<AnswerInput> getAnswerInputs() {
        return answerInputs;
    }

    public void setAnswerInputs(Set<AnswerInput> answerInputs) {
        this.answerInputs = answerInputs;
    }

    public Set<AnswerSelect> getAnswerSelects() {
        return answerSelects;
    }

    public void setAnswerSelects(Set<AnswerSelect> answerSelects) {
        this.answerSelects = answerSelects;
    }

    public Form getForm() {
        return form;
    }

    public void setForm(Form form) {
        this.form = form;
    }
}
