package rs.crm.survey.dataaccess.domain;

import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by Serozh on 3/6/2016.
 */
@Entity(name = "html_select_option")
public class Option implements Serializable {

    @Id
    @GeneratedValue
    private Long id;

    @Column(nullable = false, name = "selected" ,length = 1)
    @Type(type =  "numeric_boolean")
    private boolean selected;

    @Column(nullable = false)
    private String value;

    @Column(nullable = false)
    private String content;

    @ManyToOne(optional = false,cascade = CascadeType.ALL)
    private Select select;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Select getSelect() {
        return select;
    }

    public void setSelect(Select select) {
        this.select = select;
    }


}
