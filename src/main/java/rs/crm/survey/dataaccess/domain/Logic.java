package rs.crm.survey.dataaccess.domain;

import rs.crm.survey.dataaccess.domain.lcp.InputCondition;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by Serozh on 3/6/2016.
 */
@Entity
public class Logic implements Serializable {

    @Id
    @GeneratedValue
    private Long id;

    @ManyToOne(optional = true,cascade = CascadeType.ALL)
    private Input input;

    @ManyToOne(optional = true,cascade = CascadeType.ALL)
    private Select select;

    @ManyToOne(optional = true,cascade = CascadeType.ALL)
    private FieldSet fieldset;

    @Column(nullable = false, name = "value_condition")
    private String valueCondition;

    @Column(nullable = true,length = 2, name = "input_condition")
    @Enumerated(EnumType.ORDINAL)
    private InputCondition inputCondition;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Input getInput() {
        return input;
    }

    public void setInput(Input input) {
        this.input = input;
    }

    public Select getSelect() {
        return select;
    }

    public void setSelect(Select select) {
        this.select = select;
    }

    public FieldSet getFieldset() {
        return fieldset;
    }

    public void setFieldset(FieldSet fieldset) {
        this.fieldset = fieldset;
    }

    public String getValueCondition() {
        return valueCondition;
    }

    public void setValueCondition(String valueCondition) {
        this.valueCondition = valueCondition;
    }

    public InputCondition getInputCondition() {
        return inputCondition;
    }

    public void setInputCondition(InputCondition inputCondition) {
        this.inputCondition = inputCondition;
    }
}