package rs.crm.survey.dataaccess.domain;

import rs.crm.survey.dataaccess.domain.lcp.FormEnctype;
import rs.crm.survey.dataaccess.domain.lcp.FormMethod;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Arthur on 3/5/2016.
 */
@Entity(name = "html_form")
public class Form implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue
    private Long id;

    @Column(nullable = true)
    private String action;

    @Column(nullable = true)
    private String name;

    @Column(nullable = true)
    private String description;

    @Column(nullable = false)
    @Enumerated(EnumType.ORDINAL)
    private FormEnctype enctype;

    @Column(nullable = false)
    @Enumerated(EnumType.ORDINAL)
    private FormMethod method;

    //@OneToMany(fetch = FetchType.LAZY, mappedBy = "form")
    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "form_input",
                joinColumns = { @JoinColumn(name = "form_id") },
                inverseJoinColumns = { @JoinColumn(name = "input_id") })
    private Set<Input> inputs = new HashSet<Input>();

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "form_select",
                joinColumns = { @JoinColumn(name = "form_id") },
                inverseJoinColumns = { @JoinColumn(name = "select_id") })
    private Set<Select> selects = new HashSet<Select>();

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "form_fieldset",
                joinColumns = { @JoinColumn(name = "form_id") },
                inverseJoinColumns = { @JoinColumn(name = "fieldset_id") })
    private Set<FieldSet> fieldSets = new HashSet<FieldSet>();

    @Column(nullable = false, name = "created_by")
    private Long createdBy;

    @Column(nullable = true, name = "updated_by")
    private Long updatedBy;

    @Column(nullable = false, name = "created_at")
    @Temporal(TemporalType.DATE)
    private Date createdAt;

    @Column(nullable = true, name = "updated_at")
    @Temporal(TemporalType.DATE)
    private Date updatedAt;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "form",cascade = CascadeType.ALL)
    private Set<AnswerForm> answerForms;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public FormEnctype getEnctype() {
        return enctype;
    }

    public void setEnctype(FormEnctype enctype) {
        this.enctype = enctype;
    }

    public FormMethod getMethod() {
        return method;
    }

    public void setMethod(FormMethod method) {
        this.method = method;
    }

    public Set<Input> getInputs() {
        return inputs;
    }

    public void setInputs(Set<Input> inputs) {
        this.inputs = inputs;
    }

    public Set<Select> getSelects() {
        return selects;
    }

    public void setSelects(Set<Select> selects) {
        this.selects = selects;
    }

    public Set<FieldSet> getFieldSets() {
        return fieldSets;
    }

    public void setFieldSets(Set<FieldSet> fieldSets) {
        this.fieldSets = fieldSets;
    }

    public Long getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Long createdBy) {
        this.createdBy = createdBy;
    }

    public Long getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(Long updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<AnswerForm> getAnswerForms() {
        return answerForms;
    }

    public void setAnswerForms(Set<AnswerForm> answerForms) {
        this.answerForms = answerForms;
    }

    @Override
    public String toString() {
        return "Form{" +
                "id=" + id +
                ", action='" + action + '\'' +
                ", description='" + description + '\'' +
                ", enctype=" + enctype +
                ", method=" + method +
                ", inputs=" + inputs +
                ", selects=" + selects +
                ", fieldSets=" + fieldSets +
                ", createdBy=" + createdBy +
                ", updatedBy=" + updatedBy +
                ", createdAt=" + createdAt +
                ", updatedAt=" + updatedAt +
                '}';
    }
}
