package rs.crm.survey.dataaccess.domain.lcp;

/**
 * Created by Arthur on 3/5/2016.
 */

public enum InputType {

    CHECKBOX         (1, "checkbox", "Defines a checkbox"),
    RADIO            (2,"radio", "Defines a radio button"),
    SELECT           (3, "select","Defines a select box"),

    TEXT             (4, "text", "Default. Defines a single-line text field (default width is 20 characters)"),
    TEXT_AREA        (5, "textarea", "Default. Defines a multiple-line text field"),
    PASSWORD         (6, "password", "Defines a password field (characters are masked)"),
    EMAIL            (7, "email", "Defines a field for an e-mail address"),
    NUMBER           (8, "number", "Defines a field for entering a number"),
    RANGE            (9, "range", "Defines a control for entering a number whose exact value is not important (like a slider control)"),
    SEARCH           (10, "search", "Defines a text field for entering a search string"),
    TEL              (11, "tel", "Defines a field for entering a telephone number"),
    URL              (12, "url", "Defines a field for entering a URL"),

    DATE             (13, "date", "Defines a date control (year, month and day (no time))"),
    DATETIME         (14, "datetime", "The input type datetime has been removed from the HTML standard. Use datetime-local instead."),
    DATETIME_LOCAL   (15, "datetime_local", "Defines a date and time control (year, month, day, hour, minute, second, and fraction of a second (no time zone)"),
    MONTH            (16, "month", "Defines a month and year control (no time zone)"),
    TIME             (17, "time", "Defines a control for entering a time (no time zone)"),
    WEEK             (18, "week", "Defines a week and year control (no time zone)"),

    COLOR            (19, "color", "Defines a color picker"),

    FILE             (20, "file", "Defines a file-select field and a 'Browse...' button (for file uploads)"),
    HIDDEN           (21, "hidden", "Defines a hidden input field"),
    IMAGE            (22, "image", "Defines an image as the submit button"),

    SUBMIT           (23, "submit", "Defines a submit button"),
    RESET            (24, "reset", "Defines a reset button (resets all form values to default values)"),
    BUTTON           (25, "button", "Defines a clickable button (mostly used with a JavaScript to activate a script)");

    InputType(int id, String name, String description) {
        this.id = id;
        this.name = name;
        this.description = description;
    }

    public static InputType getDefault() {
        return TEXT;
    }

    public static synchronized InputType valueOf(final int id) {
        for (InputType i : InputType.values()) {
            if (i.getId() == id) {
                return i;
            }
        }
        return null;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    private int id;
    private String name;
    private String description;

}
