package rs.crm.survey.util;

import rs.crm.survey.dataaccess.domain.*;
import rs.crm.survey.dataaccess.domain.lcp.FormEnctype;
import rs.crm.survey.dataaccess.domain.lcp.FormMethod;
import rs.crm.survey.dataaccess.domain.lcp.InputCondition;
import rs.crm.survey.dataaccess.domain.lcp.InputType;

import java.util.*;

/**
 * Created by Serozh on 3/7/2016.
 */
public class DomainInitializer {

    public static Form initForm() {

        String uuid = UUID.randomUUID().toString();
        Date now = new Date(System.currentTimeMillis());

        Form form = new Form();
        form.setCreatedAt(now);
        form.setCreatedBy(1L);
        form.setAction("Action " + uuid);
        form.setDescription("Desc " + uuid);
        form.setName("Name " + uuid);
        form.setMethod(FormMethod.POST);
        form.setEnctype(FormEnctype.APPLICATION_X_WWW_FORM_URL_ENCODED);

        Set<Input> inputs = new HashSet<Input>();
        Set<Input> logicInputs = new HashSet<Input>();
        Set<Logic> logics = new HashSet<Logic>();

        Set<Select> selects = new HashSet<Select>();
        Set<FieldSet> fieldSets = new HashSet<FieldSet>();

        for (int i = 0; i < 5; i++) {
            inputs.add(initInput());
            selects.add(initSelect());
            fieldSets.add(initFieldSet());
        }

        List<Input> list = new ArrayList<Input>();
        list.addAll(inputs);
        for (int i = 0; i < 2; i++) {
            Logic logic = initLogic(list.get(0));
            logics.add(logic);
            logicInputs.add(initInput(logics));
        }
        inputs.addAll(logicInputs);

        form.setInputs(inputs);
        form.setSelects(selects);
        form.setFieldSets(fieldSets);

        return form;
    }

    public static FieldSet initFieldSet() {

        FieldSet fieldSet = new FieldSet();
        fieldSet.setLegend("A legend " + System.currentTimeMillis());

        Set<Input> inputs = new HashSet<Input>();
        Set<Select> selects = new HashSet<Select>();

        for (int i = 0; i < 10; i++) {
            inputs.add(initInput());
            selects.add(initSelect());
        }

        fieldSet.setInputs(inputs);
        fieldSet.setSelects(selects);

        return fieldSet;
    }

    public static Input initInput() {

        String aS = "Input " + System.currentTimeMillis();
        Boolean f = Boolean.FALSE;

        Input input = new Input();
        input.setDisabled(f);
        input.setLabel(aS);
        input.setMax(111);
        input.setMaxLength(1);
        input.setMin(1);
        input.setName(aS);
        input.setPattern(aS);
        input.setPlaceholder(aS);
        input.setReadonly(f);
        input.setRequired(f);
        input.setDefaultValue(aS);
        input.setSize(1);
        input.setStep(1);
        input.setType(InputType.WEEK);
        input.setValue(aS);

        return input;
    }

    public static Input initInput(Set<Logic> logics) {

        String aS = "Input " + System.currentTimeMillis();
        Boolean f = Boolean.FALSE;

        Input input = new Input();
        input.setDisabled(f);
        input.setLabel(aS);
        input.setMax(111);
        input.setMaxLength(1);
        input.setMin(1);
        input.setName(aS);
        input.setPattern(aS);
        input.setPlaceholder(aS);
        input.setReadonly(f);
        input.setRequired(f);
        input.setDefaultValue(aS);
        input.setSize(1);
        input.setStep(1);
        input.setType(InputType.WEEK);
        input.setValue(aS);
        input.setLogics(logics);

        for(Logic l : logics){
            l.setInput(input);
        }

        return input;
    }


    public static Input initInputWithLogic(Set<Logic> logics) {

        String aS = "Input " + System.currentTimeMillis();
        Boolean f = Boolean.FALSE;

        Input input = new Input();
        input.setDisabled(f);
        input.setLabel(aS);
        input.setMax(111);
        input.setMaxLength(1);
        input.setMin(1);
        input.setName(aS);
        input.setPattern(aS);
        input.setPlaceholder(aS);
        input.setReadonly(f);
        input.setRequired(f);
        input.setDefaultValue(aS);
        input.setSize(1);
        input.setStep(1);
        input.setType(InputType.WEEK);
        input.setValue(aS);
        input.setLogics(logics);

        return input;
    }

    public static Select initSelect() {
        String uuid = UUID.randomUUID().toString();
        Select select = new Select();
        select.setMultiple(false);
        select.setName("Name " + uuid);
        select.setSize(5);

        Set<Option> options = new HashSet<>();
        for (int i = 0; i < 5; i++) {
            Option option = initOption();
            if (i == 2) {
                option.setSelected(true);
            }
            option.setSelect(select);
            options.add(option);
        }
        select.setOptions(options);
        return select;
    }

    public static Option initOption() {
        String uuid = UUID.randomUUID().toString();
        Option option = new Option();
        option.setContent("Content " + uuid);
        option.setValue("Value " + uuid);
        option.setSelected(false);
        return option;
    }

    public static Logic initLogic(Input input) {
        Logic logic = new Logic();
        String v = input.getValue();

        logic.setInput(input);
        logic.setInputCondition(InputCondition.EQUAL);
        logic.setValueCondition(v);
        return logic;
    }
}
